#!/bin/bash
export PYTHONPATH=''
source /usr/local/genome/Anaconda2-5.1.0/etc/profile.d/conda.sh
conda activate snakemake-5.7.4

snakemake \
--snakefile $1 \
--jobscript ../workflow_metagenomics/jobscript.sh \
--cluster-config ../workflow_metagenomics/cluster.json \
--dryrun \
--printshellcmds \
--verbose
