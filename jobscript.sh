#!/bin/sh
# properties = {properties}
export PYTHONPATH=''
source /usr/local/genome/Anaconda2-5.1.0/etc/profile.d/conda.sh
conda activate snakemake-5.7.4
{exec_job}
