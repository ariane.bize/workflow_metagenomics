#!/bin/bash
export PYTHONPATH=''
source /usr/local/genome/Anaconda2-5.1.0/etc/profile.d/conda.sh
conda activate snakemake-5.7.4

mkdir -p report/

snakemake \
--snakefile $1 \
--dag \
| dot -Tpdf > ./report/`basename $1 .smk`_graph.pdf
