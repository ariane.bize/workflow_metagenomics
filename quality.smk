rule fastqc:
	input:
		"DATA/raw/{reads}.fastq.gz"
	output:
		zip = "work/fastqc/{reads}_fastqc.zip",
		html = "work/fastqc/{reads}_fastqc.html"
	params:
		output = "work/fastqc/"
	shell:
		"conda activate fastqc-0.11.8 "
		"&& "
		"fastqc "
		"{input} "
		"--noextract "
		"--outdir {params.output} "
		"&& "
		"conda deactivate "

rule multiqc:
	input:
		expand("work/fastqc/{sample}_{R}_fastqc.zip", sample=config["SAMPLES"], R=["R1", "R2"]),
		expand("work/fastp/{sample}_fastp.json", sample=config["SAMPLES"])
	output:
		html = "report/multiqc_report.html",
	params:
		output = "report/"
	shell:
		"conda activate multiqc-1.8 "
		"&& "
		"multiqc "
		"--force "
		"--no-data-dir "
		"--outdir {params.output} "
		"{input} "
		"&& "
		"conda deactivate "
